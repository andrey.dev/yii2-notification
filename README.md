Установка
------------

Подключить через композер

```
  "require": {
    "basyan/notification": "*"
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "git@git.sokolov.io:aleksandr.basov/notification.git"
    }
  ]
```

Использование
------------

Добавить и настройть любой компонент через конфигурацию приложения

```php
return [
    //....
    'components' => [
        'notifier' => [
            'class' => \basyan\notification\TelegramNotifier::class,
            'token' => $_ENV['TELEGRAM_TOKEN'],
            'chatId' => $_ENV['TELEGRAM_CHAT'],
        ],
    ],
];
```

Отправить сообщение

```php
Yii::$app->telegram->message->text = 'foo bar';
Yii::$app->telegram->message->send();
...
```

Debug and test

```
docker build -t php:yii2-notification .
docker run --rm -v $(pwd):/app php:yii2-notification composer cr
docker run --rm -v $(pwd):/app php:yii2-notification ./vendor/bin/codecept run
```