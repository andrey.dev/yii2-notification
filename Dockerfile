FROM yiisoftware/yii2-php:7.2-fpm
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN composer self-update --2 && composer global remove hirak/prestissimo
