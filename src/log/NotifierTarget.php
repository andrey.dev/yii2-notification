<?php

namespace basyan\notification\log;

use basyan\notification\DefaultNotifier;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\log\Target;

/**
 * Class NotifierTarget
 * @package basyan\notification\log
 * @property-write DefaultNotifier $notifier
 */
class NotifierTarget extends Target
{
    private $notifier;

    public function setNotifier($notifier): void
    {
        $this->notifier = $notifier;
    }

    public function init()
    {
        if (!$this->notifier) {
            throw new InvalidConfigException('The "notifier" option must be set.');
        }
        $this->notifier = Instance::ensure($this->notifier, DefaultNotifier::class);
        parent::init();
    }

    public function export()
    {
        $this->notifier->message->text = implode("\n", array_map([$this, 'formatMessage'], $this->messages));
        $this->notifier->message->send();
    }
}
