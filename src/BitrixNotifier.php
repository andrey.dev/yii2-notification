<?php

namespace basyan\notification;

use yii\httpclient\Client;

/**
 * Class BitrixNotifier
 * @package basyan\notification
 * @property-write string $hookUrl
 * @property-write string $chatId
 */
class BitrixNotifier extends DefaultNotifier
{
    private $hookUrl;
    private $chatId;


    /**
     * @param string $hookUrl
     */
    public function setHookUrl(string $hookUrl): void
    {
        $this->hookUrl = $hookUrl;
    }

    /**
     * @param string $chatId
     */
    public function setChatId(string $chatId): void
    {
        $this->chatId = $chatId;
    }

    public function sendMessage(Message $message): bool
    {
        try {
            $response = (new Client())->get(
                $this->hookUrl,
                [
                    'DIALOG_ID' => $this->chatId,
                    'MESSAGE' => $message->text,
                ]
            )->send();
            if ($response->isOk) {
                return true;
            }
            $message->addErrors([$response->content]);
        } catch (\yii\httpclient\Exception $e) {
            $message->addErrors([$e->getMessage()]);
        }
        return false;
    }
}
