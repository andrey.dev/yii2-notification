<?php

namespace basyan\notification;

interface NotifierInterface
{
    public function sendMessage(Message $message): bool;
}
