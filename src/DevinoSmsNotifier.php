<?php

namespace basyan\notification;

use SmsClient\DevinoSMS\Api;
use SmsClient\DevinoSMS\Exception;
use TelegramBot\Api\BotApi;

/**
 * Class DevinoSmsNotifier
 * @package basyan\notification
 * @property-write string $login
 * @property-write string $password
 * @property-write string $from
 * @property-write string $phone
 */
class DevinoSmsNotifier extends DefaultNotifier
{
    private $login;
    private $password;
    private $from;
    private $phone;

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from): void
    {
        $this->from = $from;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function sendMessage(Message $message): bool
    {
        try {
            $devinoSMS = new Api($this->login, $this->password);
            if ($devinoSMS->send($this->from, $this->phone, mb_substr($message->text, 0, 160))) {
                return true;
            }
        } catch (Exception $e) {
            $message->addErrors([$e->getMessage()]);
        }
        return false;
    }
}
