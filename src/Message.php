<?php

namespace basyan\notification;

use yii\base\Model;

/**
 * Class Message
 * @package basyan\notification
 * @property-write DefaultNotifier $notifier
 */
class Message extends Model
{
    private $notifier;

    public $text;

    public function __construct(DefaultNotifier $notifier, $config = [])
    {
        $this->notifier = $notifier;
        parent::__construct($config);
    }

    public function send(): bool
    {
        return $this->notifier->sendMessage($this);
    }
}
