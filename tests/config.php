<?php

return [
    'id' => 'test',
    'basePath' => dirname(__DIR__),
    'components' => [
        'telegram' => [
            'class' => \basyan\notification\TelegramNotifier::class,
            'token' => $_ENV['TELEGRAM_TOKEN'],
            'chatId' => $_ENV['TELEGRAM_CHAT'],
        ],
        'devinoSms' => [
            'class' => \basyan\notification\DevinoSmsNotifier::class,
            'login' => $_ENV['DEVINO_LOGIN'],
            'password' => $_ENV['DEVINO_PASSWORD'],
            'from' => $_ENV['DEVINO_FROM'],
            'phone' => $_ENV['DEVINO_PHONE'],
        ],
        'bitrix' => [
            'class' => \basyan\notification\BitrixNotifier::class,
            'hookUrl' => $_ENV['BITRIX_HOOK'],
            'chatId' => $_ENV['BITRIX_CHAT'],
        ],
    ],
];
